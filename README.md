
# Tic Tac Toe Game

##Installation

- composer install
- php artisan key:generate
- setup .env parameters
- php artisan migrate

## Game Description
This is a simple tic tac toe game where you post X's or O's alternately and the winner is the first one to match three X's or O's vertically, horizontally or diagonally.
This game uses Laravel as a framework, MySQL as a database and Apache as a server.
The front-end part is in resources/views/game.blade.php.
The functionalities are in app/Http/Controllers/GameController.php
Routes:
	GET /play: starts the game
	POST /{id}/update: in-game action

