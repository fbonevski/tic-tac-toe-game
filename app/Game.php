<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'games';

    protected $fillable = ['positions_x', 'positions_y', 'winner'];

    public function getId(): int
    {
        return (int)$this->getAttribute('id');
    }

    public function getXPositions(): array
    {
        return explode(',', $this->getAttribute('positions_x'));
    }

    public function addXPositions(string $position)
    {
        $current = $this->getAttribute('positions_x');

        $this->setAttribute('positions_x', $current . ',' . $position);
    }

    public function setXPositions(array $xPositions)
    {
        $this->setAttribute('positions_x', implode(',', $xPositions));
    }

    public function setOPositions(array $xPositions)
    {
        $this->setAttribute('positions_o', implode(',', $xPositions));
    }

    public function setWinner(string $winner)
    {
        $this->setAttribute('winner', $winner);
    }

    public function getWinner()
    {
        return $this->getAttribute('winner');
    }

    public function getOPositions(): array
    {
        return explode(',', $this->getAttribute('positions_o'));
    }

    public function addOPositions(string $position)
    {
        $current = $this->getAttribute('positions_o');

        $this->setAttribute('positions_o', $current . ',' . $position);
    }
}
