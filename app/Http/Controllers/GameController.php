<?php


namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Exception;

class GameController extends BaseController
{
    public function play(Request $request)
    {
        if ($id = $request->get('id', null)) {
            $game = Game::find($id);
        } else {
            $game = Game::whereNull('winner')->orderBy('created_at', 'DESC')->first();
        }

        if (!$game) {
            $game = new Game();
            $game->save();
        }

        return view('game', ['game' => $game]);
    }

    public function addPosition(int $id,Request $request)
    {
        try {
            /** @var Game $game */
            $game = Game::find($id);
            $positions = array_filter($request->all());
            unset($positions["_token"]);

            $xPositions = $game->getXPositions();
            $oPositions = $game->getOPositions();

            foreach ($positions as $position => $type) {
                $this->validateType($type);
//                $this->validateNextMove($xPositions, $oPositions, $type);

                list($xPositions, $oPositions) = $this->updatePositions($xPositions, $oPositions, $type, $position);

                $winner = $this->validateWinner($xPositions, $oPositions);

                if (count($xPositions))
                    $game->setXPositions($xPositions);

                if (count($oPositions))
                    $game->setOPositions($oPositions);

                if ($winner) {
                    $game->setWinner($winner);
                }

                $game->save();

                return redirect()->route('game.play', ['id' => $game->getId()]);
            }
        } catch (Exception $e) {
            session()->put('error', $e->getMessage());

            return redirect()->back();
        }
    }

    private function validateType(string $type)
    {
        $availableInputs = ['X', 'O'];
        if (!in_array(strtoupper($type), $availableInputs)) {
            throw new Exception("Invalid input.");
        }

        return true;
    }

    public function validateNextMove(array $xPositions, array $oPositions, string $type)
    {
        $xCount = count($xPositions);
        $oCount = count($oPositions);

        if ($xCount == $oCount) {
            return true;
        }

        if ($xCount > $oCount && strtoupper($type) == "X") {
            throw new Exception("Invalid next move.");
        }

        if ($oCount < $oCount && strtoupper($type) == "O") {
            throw new Exception("Invalid next move.");
        }

        return true;
    }

    private function updatePositions(array $xPositions, array $oPositions, string $type, $position): array
    {
        $type = strtoupper($type);

        if ($type == "X") {
            array_push($xPositions, (int)$position);
        }

        if ($type == "O") {
            array_push($oPositions, (int)$position);
        }

        return [array_filter($xPositions), array_filter($oPositions)];
    }

    private function validateWinner(array $xPositions, array $oPositions)
    {
        foreach ($this->getWinningPositions() as $winningPosition) {
            if (
                in_array($winningPosition[0], $xPositions) &&
                in_array($winningPosition[1], $xPositions) &&
                in_array($winningPosition[2], $xPositions)
            ) {
                return 'X';
            }

            if (
                in_array($winningPosition[0], $oPositions) &&
                in_array($winningPosition[1], $oPositions) &&
                in_array($winningPosition[2], $oPositions)
            ) {
                return 'O';
            }
        }

        return '';
    }

    private function getWinningPositions()
    {
        return [
            [1,2,3],
            [4,5,6],
            [7,8,9],
            [1,4,7],
            [2,5,8],
            [3,6,9],
            [1,5,9],
            [3,5,7]
        ];
    }
}
