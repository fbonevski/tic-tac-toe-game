@extends('layout')

@section('content')
    Insert X or O alternately
    <br>
    <hr style="width: 50%; float: left">
    <br>
    @if(session()->get('error'))
        <div style="border-radius: 5px; background-color: red; padding: 10px; width: 50%">
            {{session()->remove('error')}}
        </div>
    @endif
    <form action="{{route('game.add-position', $game->id)}}" method="POST">
        @csrf
        @for($i = 1; $i < 10; $i++)
            <?php
                $type = '';
                if (in_array($i, $game->getXPositions())) {
                    $type = 'X';
                }

                if (in_array($i, $game->getOPositions())) {
                    $type = 'O';
                }
            ?>
            <input type="text" value="{{$type}}" {{$type ? "disabled" : ""}} name="{{$i}}">
            @if ($i % 3 == 0)
                <br/>
            @endif
        @endfor
        @if ($game->getWinner())
            <h3>The winner is {{$game->getWinner()}}</h3>
            <a href="{{route('game.play')}}">New game</a>
        @else
            <input type="submit" value="Save" style="margin-top: 10px">
        @endif
    </form>
@stop
