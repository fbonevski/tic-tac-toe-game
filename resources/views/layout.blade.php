<html>
<head>
    <title>App Name - @yield('title')</title>
</head>
<body>
@section('sidebar')
    <h1>This is a Tic Tac Toe game</h1>
@show

<div class="container">
    @yield('content')
</div>
</body>
</html>
